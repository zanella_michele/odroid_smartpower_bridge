#ifndef HID_PNP_H
#define HID_PNP_H

#include <QObject>
#include <QTimer>
#include <QDate>
#include <QTime>
#include "../HIDAPI/hidapi.h"

#include <wchar.h>
#include <string.h>
#include <stdlib.h>
#include <fstream>

#include <time.h>

#define MAX_STR 65

class HID_PnP : public QObject
{
    Q_OBJECT
public:
    explicit HID_PnP(QObject *parent = 0);
    ~HID_PnP();
	
    void stop_sampling();
	
    void set_filename(char* file_name="log.csv"){
	log_file_name = file_name;
    };

signals:
    void hid_comm_data_update(unsigned char* raw);
    void hid_comm_status_update(bool isConnected, bool isOn, bool isStart);
    void hid_comm_version_update(unsigned char* raw);

public slots:
    void toggle_onoff();
    void toggle_startstop();
    void PollUSB();

private:
    // Device status
    hid_device *device;
    bool isConnected;
    bool onOffStatus;
    bool startStopStatus;
    bool toggleStartStop;
    bool toggleOnOff;

    // Sampling
    //QTimer *timer;
    int current_timeout;
    bool skip;
    int lastCommand;
    unsigned char buf[MAX_STR];
    unsigned char buf2[MAX_STR];
    int count;

    // Timing
    time_t start_time;
    time_t stop_time;
    double duration;
    
    // Logging
    std::ofstream log_file;
    char* log_file_name;
    char voltage[7],current[7],power[7],energy[7];
    
    void start_sampling();
    void CloseDevice();

    void save_data(unsigned char* buf);
    char * getDtTm (char *buff);
	
    
};

#endif // HID_PNP_H
